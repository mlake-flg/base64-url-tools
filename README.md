# Base64 URL-Safe Tools
This library provides a simple URL-safe Base64 encoder/decoder. Nothing fancy, just PHP's standard `base64_encode()` and `base64_decode()` functions converted to a URL- and filename-safe alphabet as defined in [RFC 4648](https://tools.ietf.org/html/rfc4648). None of this is new, I just wanted to have a convenient place to bundle these tools into my projects.

## Encoding/Decoding Data
Simply pass your raw or encoded data into the appropriate function:
```php
Base64Url::encode("Hello, world!") => "SGVsbG8sIHdvcmxkIQ"  // *see note
Base64Url::decode("SGVsbG8sIHdvcmxkIQ") => "Hello, world!"
```
_*Note: The Base64 padding characters (=) are removed from the encoded string, but are reconstituted on decode if needed._
